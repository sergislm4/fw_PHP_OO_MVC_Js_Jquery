<?php  
use \Firebase\JWT\JWT;

    class controller_login { 
        public function __construct() {
            $_SESSION['module'] = "login";
            require_once(UTILS_LOGIN . "functions.inc.php");
            include_once (LIBS . 'password_compat-master/lib/password.php');
            include_once (LIBS . "php-jwt-master/src/JWT.php");
            include_once (UTILS . 'upload.inc.php');
            include_once (UTILS . 'utils.inc.php');
        }
        
        //Views

        public function get_in() {
            require_once(VIEW_PATH_INC."header.php"); 
			require_once(VIEW_PATH_INC."menu.php");
            
            loadView(LOGIN_VIEW_PATH, 'login.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }

        public function sign_up() {
            require_once(VIEW_PATH_INC."header.php"); 
            require_once(VIEW_PATH_INC."menu.php");
            
            loadView(LOGIN_VIEW_PATH, 'register.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }

        public function restore() {
            require_once(VIEW_PATH_INC."header.php"); 
            require_once(VIEW_PATH_INC."menu.php");
            
            loadView(LOGIN_VIEW_PATH, 'restore.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }

        public function change_pass() {
            require_once(VIEW_PATH_INC."header.php"); 
            require_once(VIEW_PATH_INC."menu.php");
            
            loadView(LOGIN_VIEW_PATH, 'change_password.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }

        public function profile() {
            require_once(VIEW_PATH_INC."header.php"); 
            require_once(VIEW_PATH_INC."menu.php");
            
            loadView(LOGIN_VIEW_PATH, 'profile.html');
            
            require_once(VIEW_PATH_INC."footer.html");
        }

        //Functions to achieve register properly

        public function register() {
            $jsondata = array();

            $user = json_decode($_POST['register_json'], true);
            $key = 'MayThe4thBeWithYou';
            // echo json_encode($user);
            // exit();
            $issuedAt   = time();
            $notBefore  = $issuedAt + 1000000;
            $email = $user['email'];
            $token = array(
             "id" => "2",
             "name" => $email,
             "iat" => $issuedAt,
             "nbf" => $notBefore
            );
            $jwt = JWT::encode($token, $key, 'HS256');
            $avatar = "https://robohash.org/set_set3/bgset_bg1/". $email .".png?size=40x40";
            if ($user) {
                $arrArgument = array(
                    'email' => $user['email'],
                    'password' => password_hash($user['password'], PASSWORD_BCRYPT),
                    'birthdate' => strtoupper($user['birthdate']),
                    'avatar' => $avatar,
                    'token' => $jwt
                );
            }


            $arrVal = array(
                    'field' => "email",
                    'value' => $arrArgument['email']
                );
            $rdo = loadModel(MODEL_LOGIN, "login_model", "db_validate", $arrVal);
            

            if (!$rdo) {
                try {
                    //loadModel
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                
                if ($arrValue) {
                    sendtoken($arrArgument, "alta");

                    $url = amigable('?module=main&function=begin&param=reg');
                    echo json_encode($url);
                    exit;
                } else {
                    $url = amigable('?module=main&function=begin&param=503');
                    echo json_encode($url);
                }
            } else {
                echo json_encode(2);
            }
        }

        function social_signin() { //utilitzada per Facebook i Twitter
            $user = json_decode($_POST['user'], true);
            // echo json_encode($user);
            // exit();
            try {
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "count", array('column' => array('user_id'), 'like' => array($user['token'])));
            } catch (Exception $e) {
                $arrValue = false;
            }

            if (!$arrValue[0]["total"]) {
                $arrArgument = array(
                    'user_id' => $user['token'],
                    'email' => $user['email'],
                    'avatar' => $user['avatar']
                );

                try {
                    $value = loadModel(MODEL_LOGIN, "login_model", "create_social", $arrArgument);
                } catch (Exception $e) {
                    $value = false;
                }
            } else
                $value = true;

            if ($value) {
                $arrArgument = array(
                    'column' => array("user_id"),
                    'like' => array($user['token']),
                    'field' => array('*')
                );
                $user = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);
                echo json_encode($user);
            } else {
                echo json_encode(array('error' => true, 'data' => 503));
            }
        }

        public function db_validate() {
            $field = $_POST['field_to_evaluate'];
            $value = $_POST['value_to_evaluate'];
            // echo json_encode($value);
            // exit();
            $arrArgument = array(
                    'field' => $field,
                    'value' => $value
                );
            $rdo = loadModel(MODEL_LOGIN, "login_model", "db_validate", $arrArgument);
            echo json_encode($rdo);
            // exit();
        }
        public function display_menu() {
            $key = 'MayThe4thBeWithYou';
            $arrArgument =$_POST['token'];
            // $decoded = JWT::decode($arrArgument, $key, 'HS256');
            // echo json_encode($decoded);
            // exit();
            $rdo = loadModel(MODEL_LOGIN, "login_model", "display_menu", $arrArgument);
            echo json_encode($rdo);
            exit();

        }

        function verify() {
            if ($_GET['param']) {
                // $arrArgument = $_GET['param'];
                $arrArgument = array(
                    'column' => array('user_id'),
                    'like' => array($_GET['param']),
                    'field' => array('activated'),
                    'new' => array('1')
                );
                // print_r($arrArgument);
                // die();
                try {
                    $value = loadModel(MODEL_LOGIN, "login_model", "update", $arrArgument);
                } catch (Exception $e) {
                    $value = false;
                }

                if ($value) {
                   //  print_r($value);
                   // die();
                    redirect('http://localhost/fw_PHP_OO_MVC_Js_Jquery/main/begin/verified/');
                    // require_once(VIEW_PATH_INC."header.php"); 
                    // require_once(VIEW_PATH_INC."menu.php");

                    // loadView('modules/main/view/', 'main.html');

                    // require_once(VIEW_PATH_INC."footer.html");

                } else {
                    redirect('http://localhost/fw_PHP_OO_MVC_Js_Jquery/main/begin/503/');
                   // require_once(VIEW_PATH_INC."header.php"); 
                   // require_once(VIEW_PATH_INC."menu.php");

                   // loadView('view/inc/', '404.php');

                   // require_once(VIEW_PATH_INC."footer.html");
                }
            }
        }

        //Functions to achieve login properly
        public function login() {
            $user = json_decode($_POST['login_json'], true);
            $arrArgument = array(
                'column' => array('email'),
                'like' => array($user['email']),
                'field' => array('password')
            );
            // print_r($arrArgument);
            // die();

            try {
                //loadModel
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);
                $arrValue = password_verify($user['pass'], $arrValue[0]['password']);
            } catch (Exception $e) {
                $arrValue = "error";
            }

            if ($arrValue !== "error") {
                if ($arrValue) { 
                    try {
                        $arrArgument = array(
                            'column' => array("email", "activated"),
                            'like' => array($user['email'], "1")
                        );
                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "count", $arrArgument);

                        if ($arrValue[0]["total"] == 1) {
                            $arrArgument = array(
                                'column' => array("email"),
                                'like' => array($user['email']),
                                'field' => array('user_id')
                            );
                            $user = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);
                            echo json_encode($user);
                            exit();
                        } else {
                            $value = array(
                                "error" => true,
                                "data" => "User has not been activated, please check your email!"
                            );
                            echo json_encode($value);
                            exit();
                        }
                    } catch (Exception $e) {
                        $value = array(
                            "error" => true,
                            "data" => 503
                        );
                        echo json_encode($value);
                    }
                } else {
                    $value = array(
                        "error" => true,
                        "data" => "Email and password doesn't match"
                    );
                    echo json_encode($value);
                }
            } else {
                $value = array(
                    "error" => true,
                    "data" => 503
                );
                echo json_encode($value);
            }
        }

        //Restore Process

        public function process_restore() {
            $result = array();
            if (isset($_POST['inputEmail'])) {
                $result = validatemail($_POST['inputEmail']);
                if ($result) {
                    $column = array(
                        'email'
                    );
                    $like = array(
                        $_POST['inputEmail']
                    );
                    $field = array(
                        'user_id'
                    );
                    $email = $_POST['inputEmail'];
                    $key = 'MayThe4thBeWithYou';
                    $issuedAt   = time();
                    $notBefore  = $issuedAt + 1000000;
                    $email = $user['email'];
                    $token = array(
                     "id" => "2",
                     "name" => $email,
                     "iat" => $issuedAt,
                     "nbf" => $notBefore
                    );
                    $jwt = JWT::encode($token, $key, 'HS256');
                    $new = array(
                        $jwt
                    );

                    $arrArgument = array(
                        'column' => $column,
                        'like' => $like,
                        'field' => $field,
                        'new' => $new
                    );
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "count", $arrArgument);
                    if ($arrValue[0]['total'] == 1) {
                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "update", $arrArgument);
                        if ($arrValue) {
                            //////////////// Envio del correo al usuario
                            $arrArgument = array(
                                'token' => $jwt,
                                'email' => $_POST['inputEmail']
                            );
                            if (sendtoken($arrArgument, "modificacion"))
                                echo "Your new password has been sent to your email";
                            else
                                echo "Server error! Try again later. ";
                        }
                    } else {
                        echo "Introduced email does not exists ";
                    }
                } else {
                    echo "Invalid email";
                }
            }
        }

        function update_pass() {
            $jsondata = array();
            $pass = json_decode($_POST['password'], true);
            $arrArgument = array(
                'column' => array('user_id'),
                'like' => array($pass['token']),
                'field' => array('password'),
                'new' => array(password_hash($pass['password'], PASSWORD_BCRYPT))
            );
            // echo json_encode($arrArgument);
            // exit();
            try {
                $value = loadModel(MODEL_LOGIN, "login_model", "update", $arrArgument);
            } catch (Exception $e) {
                $value = false;
            }

            if ($value) {
                $url = amigable('?module=main&function=begin&param=rest', true);
                $jsondata["success"] = true;
                $jsondata["redirect"] = $url;
                echo json_encode($jsondata);
                exit();
            } else {
                $url = amigable('?module=main&function=begin&param=503', true);
                $jsondata["success"] = true;
                $jsondata["redirect"] = $url;
                echo json_encode($jsondata);
                exit();
            }
        }

        // Profile operations

        function profile_filler() {
            if (isset($_POST['user'])) {
                try {
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "select", array(column => array('user_id'), like => array($_POST['user']), field => array('*')));
                } catch (Exception $e) {
                    $arrValue = false;
                }

                if ($arrValue) {
                    $jsondata["success"] = true;
                    $jsondata['user'] = $arrValue[0];
                    echo json_encode($jsondata);
                    exit();
                } else {
                    $url = amigable('?module=main', true);
                    $jsondata["success"] = false;
                    $jsondata['redirect'] = $url;
                    echo json_encode($jsondata);
                    exit();
                }
            } else {
                $url = amigable('?module=main', true);
                $jsondata["success"] = false;
                $jsondata['redirect'] = $url;
                echo json_encode($jsondata);
                exit();
            }
        }
        function upload_avatar() {
            $result_avatar = upload_files();
            $_SESSION['avatar'] = $result_avatar;
        }
        
        function delete_avatar() {
            $_SESSION['avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }

        function load_pais_user() {
            if ((isset($_GET["param"])) && ($_GET["param"] == true)) {
                $json = array();
                $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
                try {
                    $json = loadModel(MODEL_LOGIN, "login_model", "obtain_paises", $url);
                } catch (Exception $e) {
                    $json = false;
                }

                if ($json) {
                    if (preg_match('/Error/',$json)) {
                      $json = "error";
                    }
                    echo $json;
                } else {
                    $json = "error";
                    echo $json;
                    exit;
                }
            }
        }

        function load_provincias_user() {
            if ((isset($_GET["param"])) && ($_GET["param"] == true)) {
                $jsondata = array();
                $json = array();

                try {
                    $json = loadModel(MODEL_LOGIN, "login_model", "obtain_provincias");
                } catch (Exception $e) {
                    $json = false;
                }

                if ($json) {
                    $jsondata["provincias"] = $json;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $jsondata["provincias"] = "error";
                    echo json_encode($jsondata);
                    exit;
                }
            }
        }

        function load_poblaciones_user() {
            if (isset($_POST['idPoblac'])) {
                $jsondata = array();
                $json = array();

                try {
                    $json = loadModel(MODEL_LOGIN, "login_model", "obtain_poblaciones", $_POST['idPoblac']);
                } catch (Exception $e) {
                    $json = false;
                }

                if ($json) {
                    $jsondata["poblaciones"] = $json;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $jsondata["poblaciones"] = "error";
                    echo json_encode($jsondata);
                    exit;
                }
            }
        }

        function modify() {
            $jsondata = array();
            $userJSON = json_decode($_POST['mod_user_json'], true);

            if ($userJSON) {
                $arrArgument = array(
                    'name' => $userJSON['nombre'],
                    'surnames' => $userJSON['apellidos'],
                    'email' => $userJSON['email'],
                    'password' => password_hash($userJSON['password'], PASSWORD_BCRYPT),
                    'birthdate' => strtoupper($userJSON['date_birthday']),
                    'bank' => $userJSON['bank'],
                    'avatar' => $_SESSION['avatar']['data'],
                    'dni' => $userJSON['dni'],
                    'country' => $userJSON['pais'],
                    'province' => $userJSON['provincia'],
                    'town' => $userJSON['poblacion']
                );

                $arrayDatos = array(
                    column => array(
                        'email'
                    ),
                    like => array(
                        $arrArgument['email']
                    )
                );

                $j = 0;
                foreach ($arrArgument as $clave => $valor) {
                    if ($valor != "") {
                        $arrayDatos['field'][$j] = $clave;
                        $arrayDatos['new'][$j] = $valor;
                        $j++;
                    }
                }

                try {
                    // echo json_encode($arrayDatos);
                    // exit();
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "update", $arrayDatos);
                } catch (Exception $e) {
                    $arrValue = false;
                }

                if ($arrValue) {
                    $url = amigable('?module=login&function=profile&param=done', true);
                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $url;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $jsondata["success"] = false;
                    $jsondata["redirect"] = $url = amigable('?module=login&function=profile&param=503', true);
                    echo json_encode($jsondata);
                }

            } else {
                $jsondata["success"] = false;
                $jsondata['data'] = $result;
                echo json_encode($jsondata);
            }
        }
    }